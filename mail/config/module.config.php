<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:04 PM
 */
return array(
    'mail'=>array(
        /* Sample: Should really be in the application config itself

        'enabled'=>true,
        'override'=>array(
            'enabled'=>true,
            'address'=>'derekmiranda@me.com',
        ),
        'layouts'=>array(
            'html'=>'application/layout/email.phtml',
            'text'=>'application/layout/email.txt',
        ),
        'connection'=>array(
            'name' => 'smtp.emailsrvr.com',
            'host' => 'smtp.emailsrvr.com',
            'port' => 587,
            'connection_class'=>'login',
            'connection_config'=>array(
                'username' => 'derek@zdidesign.com',
                'password' => 'Dwm09058',
            ),
        ),
        'configs'=>array(
            'defaults'=>array(
                'to'=>'derek@zdidesign.com',
                'from'=>'derek@zdidesign.com',
            ),
            'contact'=>array(
                'html_template'=>'contact.phtml',
                'text_template'=>'contact.txt',
                'to'=>'test@test.com',
                'from'=>'derek@zdidesign.com',
                'subject'=>'This is my Subject',
            ),
        ),*/
    ),
    'service_manager'=>array(
        'invokables'=>array(
            'Mail'=>'Mail\Mail\Mail',
        ),
        'factories'=>array(
            'MailTransport'=>function($sm)
            {
                $config = $sm->get('config');
                $transport = new \Zend\Mail\Transport\Smtp();
                $options = new \Zend\Mail\Transport\SmtpOptions($config['mail']['connection']);
                $transport->setOptions($options);
                return $transport;
            }
        ),
    ),
);