<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/10/14
 * Time: 6:49 PM
 */

namespace Mail\Mail;
use Zend\Mime\Mime;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Mail\Message;
use Zend\Mime\Part;

class Mail implements ServiceLocatorAwareInterface {

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * To Addresses
     * @var array
     */
    protected $to;

    /**
     * From Address
     * @var string
     */
    protected $from;

    /**
     * Subject
     * @var string
     */
    protected $subject;

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param array $to
     */
    public function setTo($to)
    {
        $config = $this->getServiceLocator()->get('config');

        if( array_key_exists('override', $config['mail']))
        {
            if( $config['mail']['override']['enabled'] == true )
            {
                $to = $config['mail']['override']['address'];
            }
        }

        $this->to = $to;
    }

    /**
     * Sends the message
     * @param $key
     * @param array $data
     * @throws \Zend\View\Exception\RuntimeException
     * @throws \Zend\View\Exception\DomainException
     * @throws \Exception
     */
    public function send($key, $data = array())
    {
        $renderer = $this->getServiceLocator()->get('ViewRenderer');
        $config = $this->getServiceLocator()->get('config');

        if( ! array_key_exists('mail', $config) )
        {
            throw new \Exception('Could not retrieve mail configuration');
        }

        if( $config['mail']['enabled'] == false )
        {
            return;
        }

        $messageConfig = @$config['mail']['configs'][$key];

        if( ! is_array($messageConfig) )
        {
            throw new \Exception('Could not retrieve message configuration for ' . $key);
        }

        $this->prepareEssentials($messageConfig);

        if( ! array_key_exists('html_template', $messageConfig) && ! array_key_exists('text_template', $messageConfig))
        {
            throw new \Exception('No templates found for ' . $key);
        }

        $htmlContent = null;
        $textContent = null;

        if( is_array($config['mail']['layouts']) )  // render with layouts
        {
            // HTML
            if (array_key_exists('html_template', $messageConfig))
            {
                if (isset($config['mail']['layouts']['html']))
                {
                    $templateToRender = $config['mail']['layouts']['html'];
                    $data['innerTemplate'] = $messageConfig['html_template'];
                }
                else
                {
                    $templateToRender = $messageConfig['html_template'];
                }

                $htmlContent = $renderer->render($templateToRender, $data);
            }

            // TEXT
            if (array_key_exists('text_template', $messageConfig))
            {
                if (isset($config['mail']['layouts']['text']))
                {
                    $templateToRender = $config['mail']['layouts']['text'];
                    $data['innerTemplate'] = $messageConfig['text_template'];
                }
                else
                {
                    $templateToRender = $messageConfig['text_template'];
                }

                $textContent = $renderer->render($templateToRender, $data);
            }
        }
        else // render without layouts
        {
            if( array_key_exists('html_template', $messageConfig))
            {
                $htmlContent = $renderer->render($messageConfig['html_template'], $data);
            }

            if( array_key_exists('text_template', $messageConfig))
            {
                $textContent = $renderer->render($messageConfig['text_template'], $data);
            }
        }

        $parts = array();

        if( $htmlContent !== null )
        {
            $htmlPart = new Part($htmlContent);
            $htmlPart->type = Mime::TYPE_HTML;
            $parts[] = $htmlPart;
        }

        if( $textContent !== null )
        {
            $textPart = new Part($textContent);
            $textPart->type = Mime::TYPE_TEXT;
            $parts[] = $textPart;
        }

        $mimeMessage = new \Zend\Mime\Message();
        $mimeMessage->setParts($parts);

        $message = new Message();
        $message->addTo($this->getTo());
        $message->setFrom($this->getFrom());
        $message->setSubject($this->getSubject());
        $message->setBody($mimeMessage);
        $message->setEncoding('UTF-8');

        $transport = $this->getServiceLocator()->get('MailTransport');
        $transport->send($message);
    }

    /**
     * Ensures that the to and from addresses are set
     * @param $config
     * @throws \Exception
     */
    protected function prepareEssentials($config)
    {
        if( $this->to == null )
        {
            if( array_key_exists('to', $config))
            {
                $this->setTo($config['to']);
            }
            else
            {
                $defaults = $this->getDefaults();
                $this->setTo($defaults['to']);
            }
        }

        if( $this->from == null )
        {
            if( array_key_exists('from', $config))
            {
                $this->setFrom($config['from']);
            }
            else
            {
                $defaults = $this->getDefaults();
                $this->setFrom($defaults['from']);
            }
        }

        if( $this->subject == null )
        {
            if( array_key_exists('subject', $config))
            {
                $this->setSubject($config['subject']);
            }
        }
    }

    /**
     * Resets the base essential fields
     */
    public function reset()
    {
        $this->to = null;
        $this->from = null;
        $this->subject = null;
    }

    /**
     * Gets the message defaults
     * @return mixed
     * @throws \Exception
     */
    protected function getDefaults()
    {
        $config = $this->getServiceLocator()->get('config');
        $defaults = $config['mail']['configs']['defaults'];

        if( ! is_array($defaults) )
        {
            throw new \Exception('Could not retrieve message defaults!');
        }

        return $defaults;
    }
} 