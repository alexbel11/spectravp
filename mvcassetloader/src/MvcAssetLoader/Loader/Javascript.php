<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/12/14
 * Time: 9:35 PM
 */

namespace MvcAssetLoader\Loader;

class Javascript extends AbstractLoader implements LoaderInterface {

    /**
     * Attaches all the files to the headlink
     * @throws \Exception
     */
    public function attach()
    {
        $config = $this->getConfig();

        if( ! isset($config['javascript']))
        {
            throw new \Exception('Error: No Javascript config set!');
        }

        $config = $config['javascript'];

        $extension = '.js';
        $directory = '/js/';
        $absolutePath = $this->getAbsolutePath();

        if( array_key_exists('minified', $config) && $config['minified'] === true)
        {
            $extension = '.min' . $extension;
        }

        if( array_key_exists('directory', $config))
        {
            $directory = $config['directory'];
        }

        $headlink = $this->getServiceLocator()->get('viewhelpermanager')->get('headScript');

        foreach($this->getLibraries() as $library => $settings)
        {
            if( $settings === true || (is_array($settings) && isset($settings['load']) && $settings['load'] === true ))
            {
                $target = $this->resolveFile($absolutePath, $directory, $library, $extension, $settings);

                if( $target !== false )
                {
                    $conditional = null;

                    if( is_array($settings) && array_key_exists('conditional', $settings))
                    {
                        $conditional = $settings['conditional'];
                    }

                    $headlink->appendFile($target, 'text/javascript', $conditional, null);
                }
            }
        }
    }

    /**
     * Gets all the files to load
     * @return array
     * @throws \Exception
     */
    public function getLibraries()
    {
        $config = parent::getConfig();

        if( ! isset($config['javascript']))
        {
            throw new \Exception('Error: No Javascript config set!');
        }

        if( array_key_exists('cache', $config) && array_key_exists('enabled', $config['cache']) && $config['cache']['enabled'] )
        {
            $cache = $this->getCacheAdapter();

            if( $cache->hasItem($this->getCacheName('javascript')) )
            {
                return $cache->getItem($this->getCacheName('javascript'));
            }
        }

        $cssConfig = $config['javascript'];
        $libs = $cssConfig['libraries']['common'];

        if( $cssConfig['load_common'] )
        {
            $libs['common'] = true;
        }

        if( array_key_exists('desktop_only', $cssConfig['libraries']) || array_key_exists('mobile_only', $cssConfig['libraries']))
        {
            if( $this->getServiceLocator()->has('MobileDeviceDetector'))
            {
                $detector = $this->getServiceLocator()->get('MobileDeviceDetector');

                if (!$detector->isMobile() && array_key_exists('desktop_only', $cssConfig['libraries']))
                {
                    $libs = array_merge($libs, $cssConfig['libraries']['desktop_only']);
                }
                elseif ($detector->isMobile() && array_key_exists('mobile_only', $cssConfig['libraries']))
                {
                    $libs = array_merge($libs, $cssConfig['libraries']['mobile_only']);
                }
            }
        }

        $libs = array_merge($libs, $this->preMvcFiles);

        $libs = array_merge($libs, array($this->getModuleName() => true));
        $libs = array_merge($libs, array($this->getModuleName() . '/' . $this->getControllerName() => true));
        $libs = array_merge($libs, array($this->getModuleName() . '/' . $this->getControllerName() . '/'. $this->getActionName() => true));

        $libs = array_merge($libs, $this->postMvcFiles);

        if( array_key_exists('guides', $cssConfig['libraries']))
        {
            $libs = array_merge($libs, $cssConfig['libraries']['guides']);
        }

        if( isset($cache) )
        {
            $cache->setItem($this->getCacheName('javascript'), $libs);
        }

        return $libs;
    }
} 