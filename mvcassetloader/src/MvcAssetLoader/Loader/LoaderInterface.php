<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 4/12/14
 * Time: 9:36 PM
 */
namespace MvcAssetLoader\Loader;

interface LoaderInterface
{
    /**
     * @return array
     */
    public function getLibraries();

    /**
     * Executes the attachment
     */
    public function attach();

    /**
     * Adds a file to attatch
     * @param $file
     * @param null $conditions
     * @return mixed
     */
    public function add($file, $conditions = null);
}