<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 2:50 PM
 * Project: filter
 */
namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Json\Decode;

/**
 * Class JsonDecode
 * @package Filter
 */
class JsonDecode extends AbstractFilter {

    /**
     * @param $data
     * @return mixed
     */
    public function filter($data)
    {
        if( $data == null || $data == '' )
        {
            return '';
        }

        return Decoder::decode($data);
    }
} 