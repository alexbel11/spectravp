<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 9/9/14
 * Time: 1:21 PM
 * Project: filter
 */
namespace Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Digits;

/**
 * Class Phone
 * @package Filter
 */
class Phone extends AbstractFilter {

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $data
     * @return mixed|string
     */
    public function filter($data)
    {
        $filter = new Digits();
        $data = $filter->filter($data);

        $length = strlen($data);

        if( $length == 7 )
        {
            return substr($data, 0, 3) . '-' . substr($data, 3);
        }

        if( $length == 10 )
        {
            return '(' . substr($data, 0, 3) . ') ' . substr($data, 3, 3) . '-' . substr($data, 6);
        }

        if( $length == 11)
        {
            return substr($data, 0, 1) . ' (' . substr($data, 1, 3) . ') ' . substr($data, 4, 3) . '-' . substr($data, 7);
        }

        return $data;
    }
}