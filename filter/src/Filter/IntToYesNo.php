<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/11/13
 * Time: 9:49 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class IntToYesNo
 * @package Filter
 */
class IntToYesNo extends AbstractFilter {

    /**
     * @param $var
     * @return string
     */
    public function filter($var)
    {
        if( is_bool($var) && $var == true)
        {
            return 'Yes';
        }

        if( is_int($var) && $var >= 1)
        {
            return 'Yes';
        }

        if( is_string($var) && $var == '1')
        {
            return 'Yes';
        }

        return 'No';
    }
} 