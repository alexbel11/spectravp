<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 8/25/14
 * Time: 12:04 PM
 * Project: filter
 */
namespace Filter;

use Zend\Filter\AbstractFilter;
use Zend\Filter\Word\CamelCaseToUnderscore;
use Zend\Filter\Word\SeparatorToSeparator;

class DatabaseTableName extends AbstractFilter {

    /**
     * Filters the value into a database table name
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        $value = array_pop(explode('\\Table\\', $value));

        $filterA = new SeparatorToSeparator('\\', '_');
        $filterB = new CamelCaseToUnderscore();

        return strtolower(
            $filterB->filter(
                array_pop(explode('\\', $filterA->filter($value)))
            )
        );
    }
} 