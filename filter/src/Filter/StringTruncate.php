<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/29/13
 * Time: 6:08 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class StringLength
 * @package ClickLogical\Filter
 */
class StringTruncate extends AbstractFilter {

    /**
     * @var int
     */
    protected $stringLength;

    /**
     * @var bool
     */
    protected $appendEllipsis = true;

    /**
     * @var bool
     */
    protected $useWordWrap = true;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Filters the string
     * @param mixed $value
     * @return mixed|string
     */
    public function filter($value)
    {
        $count = strlen($value);

        if( $count < $this->stringLength )
        {
            return $value;
        }

        if( $this->useWordWrap )
        {
            $value = substr($value, 0, strpos(wordwrap($value, $this->stringLength), "\n"));
        }

        else
        {
            $value = substr($value, 0, $this->stringLength);
        }

        if( $this->appendEllipsis )
        {
            $value .= '&hellip;';
        }

        return $value;
    }

    /**
     * @return boolean
     */
    public function getAppendEllipsis()
    {
        return $this->appendEllipsis;
    }

    /**
     * @param boolean $appendEllipsis
     */
    public function setAppendEllipsis($appendEllipsis)
    {
        $this->appendEllipsis = $appendEllipsis;
    }

    /**
     * @return int
     */
    public function getStringLength()
    {
        return $this->stringLength;
    }

    /**
     * @param int $stringLength
     */
    public function setStringLength($stringLength)
    {
        $this->stringLength = $stringLength;
    }

    /**
     * @return boolean
     */
    public function getUseWordWrap()
    {
        return $this->useWordWrap;
    }

    /**
     * @param boolean $useWordWrap
     */
    public function setUseWordWrap($useWordWrap)
    {
        $this->useWordWrap = $useWordWrap;
    }
} 