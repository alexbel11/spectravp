<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 2:15 PM
 * Project: sql
 */

namespace Sql\Sql;

trait DuplicateInsert {

    /**
     * Inserts a record or updates on duplicate index
     * @param $data
     * @returns int The primary key of the record inserted or updated
     */
    public function insertOnDuplicateUpdate($data)
    {
        $insert = $this->getSql()->insert();
        $insert->values($data);
        $baseSQL = $insert->getSqlString($this->adapter->platform);

        $update = $this->getSql()->update();
        $update->set($data);
        $baseUpdate = $update->getSqlString($this->adapter->platform);

        $parts = explode('SET', $baseUpdate);

        $baseSQL .= ' ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), ' . $parts[1];

        $result = $this->getAdapter()->getDriver()->getConnection()->execute($baseSQL);
        return $result->getGeneratedValue();
    }

}